/*
 * MO_GAPruner.h
 *
 *  Created on: Mar 29, 2011
 *      Author: rgreen
 */

#ifndef MO_GAPRUNER_H_
#define MO_GAPRUNER_H_

#include "MOPruner.h"
#include "MO_Chromosome.h"

class MO_GAPruner : MOPruner{
	MO_GAPruner(int popSize, int generations, Classifier* o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul=false);
	MO_GAPruner(int popSize, int generations, double mut, double cross, Classifier* o, std::vector<Generator> g, std::vector<Line> l,
					double p, bool ul=false);
	virtual ~MO_GAPruner();

	void Init(int popSize, int generations, double mut, double cross, Classifier* o, std::vector<Generator> g, std::vector<Line> l,
				double p, bool ul=false);
	void Prune(MTRand& mt);

	void clearVectors();
	void clearTimes();
	void Reset(int np, int nt);


	protected:
		static bool sortVector(const std::vector<double>& elem1, const std::vector<double>& elem2);
		static bool sortPop(const MO_Chromosome& elem1, const MO_Chromosome elem2);

		bool isConverged();

		void initPopulation(MTRand& mt);
		void evaluateFitness();
		void updatePositions(MTRand& mt);
		void selectionAndCrossover(MTRand& mt);
		void mutate(MTRand& mt);

		CStopWatch timer1, cTimer;

		//EVALUATOR EvaluateSolution;

		double pMut, pCrossover;
		double 	initTime,  fitnessTime, selectTime, crossTime, mutTime;
		double bestFitness, totalFitness;

		std::vector <MO_Chromosome> pop;
		std::vector <MO_Chromosome> newPop;
		std::vector < std::vector < int > > toCross;
};

#endif /* MO_GAPRUNER_H_ */
