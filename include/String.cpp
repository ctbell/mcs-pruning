#include "Utils.h"
#include "String.h"
#include <iostream>
#include "Eigen/Dense"

namespace String {
	
	void tokenizeString(std::string str, std::vector<std::string>& tokens, const std::string& delimiter = " ") {

		int pos = 0;
		std::string token;

		for (;;) {

			pos = str.find(delimiter);

			if (pos == (int)std::string::npos) {
				tokens.push_back(str);
				break;
			}

			token = str.substr(0, pos);
			tokens.push_back(token);
			str = str.substr(pos + 1);
		}
	}

	std::string vectorToString(std::vector<double> v) {
		std::string result;
		char buf[1];

		result = "";
		for (unsigned int x = 0; x< v.size(); x++) {
			sprintf(buf, "%i", (int)v[x]);
			result += buf[0];
		}
		return result;
	}
	std::string vectorToString(std::vector<int> v) {
		std::string result;

		result = "";
		for (unsigned int x = 0; x< v.size(); x++) {
			if (v[x] == 1) { result += '1'; }
			else { result += '0'; }
		}
		return result;
	}

	std::string arrayToString(int* v, int length) {
		std::string result;
		char buf[1];

		result = "";
		for (int x = 0; x< length; x++) {
			sprintf(buf, "%i", v[x]);
			result += buf[0];
		}
		return result;
	}
}