#include "Utils.h"
#include "CommandLine.h"
#include <iostream>
#include "Eigen/Dense"

namespace CommandLine {
	void setUsage(AnyOption* opt) {
		// General
		opt->addUsage("");
		opt->addUsage("Usage: ");
		opt->addUsage("");
		opt->addUsage("--help              Prints this help");
		opt->addUsage("");

		// Parallelization
		opt->addUsage("--numThreads        # of threads to use with OpenMP");
		opt->addUsage("--batchSize         # of samples per batch used in MCS Sampling with OpenMP or CUDA");

		// Parallel Batch OpenMP
		opt->addUsage("--genThreads        For use with OpenMP Pipeline. Number of Generation Threads.");
		opt->addUsage("--classThreads      For use with OpenMP Pipeline. Number of Classification Threads.");

		// Power System Specific:wq
		opt->addUsage("--system            RTS79, MRTS, or RTS96. Defaults to RTS79");
		opt->addUsage("--useLines          Use Transmission Lines");
		opt->addUsage("--usePHEVs          Use PHEV formulation");
		opt->addUsage("--lineAdj           Multiplied with line FOR. Defaults to 1.0");
		opt->addUsage("--saveGenOutages    Save Generator Outage Stats");
		opt->addUsage("--saveLineOutages   Save Line Outage Stats");
		opt->addUsage("");

		// PHEV Specific
		opt->addUsage("--penetration       Penetration level for PHEVs");
		opt->addUsage("--rho               Rho value for PHEVs");
		opt->addUsage("--totalVehicles     Total Vehicles for PHEVs");
		opt->addUsage("--placement         PHEV Placement - EvenAllBuses, EvenLoadBuses, RandomAllBuses, RandomLoadBuses, FairDistribution");
		opt->addUsage("");

		// Classification
		opt->addUsage("--classifier        CAP, SVM, NN, or OPF. Defaults to OPF");
		opt->addUsage("");

		// Sampling
		opt->addUsage("--sampler           MCS, LHS, DS, HAL, HAM, FAU, SOB, or PI. Defaults to MCS.");
		opt->addUsage("--samplingPop       Sampling population size. Defaults to 25.");
		opt->addUsage("--samplingGen       Sampling number of generations. Defaults to 25.");
		opt->addUsage("--sigma             Tolerance for convergance");
		opt->addUsage("--useMOSampler      Use Multi-Objective Sampler");
		opt->addUsage("--numSamples        Number of experiments for LHS");
		opt->addUsage("");

		// Pruning
		opt->addUsage("--pruner            MCS, AIS, GA, MGA, PSO, or ACO. Defaults to MCS.");
		opt->addUsage("--useMOPruner       Use Multi-Objective Pruner");
		opt->addUsage("--useWeightedMO     Use Weighted version of Multi-Objective Pruner");
		opt->addUsage("--useParetoMO       Use Pareto version of Multi-Objective Pruner");
		opt->addUsage("--useLocalSearch    Use Local Search Algorithm");
		opt->addUsage("");
		opt->addUsage("--logConvergence    Log Convergence Characteristics");
		opt->addUsage("--negateFitness     Negate Fitness Function");
		opt->addUsage("--pruningObj        Prob, Curt, Excess, or Copy. Defaults to Prob");
		opt->addUsage("");
		opt->addUsage("--pruningPopMin     Min population sizeDefaults to 25.");
		opt->addUsage("--pruningPopMax     Max population sizeDefaults to 25.");
		opt->addUsage("--pruningPopStep    Incremental value from popMin to popMaxDefaults to 5.");
		opt->addUsage("");
		opt->addUsage("--pruningGenMin     Min number of generationsDefaults to 25.");
		opt->addUsage("--pruningGenMax     Max number of generationsDefaults to 25.");
		opt->addUsage("--pruningGenStep    Incremental value from genMin to genMax. Defaults to 5.");
		opt->addUsage("");
		opt->addUsage("--stoppingMethod    MaxProbPruned, MinProbSlope, MaxStatesPruned, MinStatesSlope, or Generation. Defaults to Generation.");
		opt->addUsage("--stoppingValue     Used in conjection with stoppingMethod.");
		opt->addUsage("--trials            Trials per run");

	}
	void setOptions(AnyOption* opt) {
		// Flags
		opt->setFlag("help", 'h');

		// Parallel
		opt->setOption("batchSize");
		opt->setOption("numThreads");

		// Parallel Batch OpenMP
		opt->setOption("genThreads");
		opt->setOption("classThreads");

		// Power System
		opt->setOption("system");
		opt->setOption("lineAdj");
		opt->setFlag("useLines");
		opt->setFlag("usePHEVs");
		opt->setFlag("saveGenOutages");
		opt->setFlag("saveLineOutages");

		// PHEV Options
		opt->setOption("penetration");
		opt->setOption("rho");
		opt->setOption("totalVehicles");
		opt->setOption("placement");

		// Classifying
		opt->setOption("classifier");

		// Sampling
		opt->setOption("sampler");
		opt->setOption("samplingPop");
		opt->setOption("samplingGen");
		opt->setOption("sigma");
		opt->setOption("numSamples");

		// Pruning
		opt->setFlag("logConvergence");
		opt->setFlag("negateFitness");
		opt->setFlag("useMOPruner");
		opt->setFlag("useWeightedMO");
		opt->setFlag("useParetoMO");
		opt->setFlag("useLocalSearch");

		opt->setOption("pruner");
		opt->setOption("pruningObj");
		opt->setOption("trials");
		opt->setOption("stoppingMethod");
		opt->setOption("stoppingValue");
		opt->setOption("pruningPopMin"); opt->setOption("pruningPopMax"); opt->setOption("pruningPopStep");
		opt->setOption("pruningGenMin"); opt->setOption("pruningGenMax"); opt->setOption("pruningGenStep");
	}
}

}