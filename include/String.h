#ifndef STRING_H_
#define STRING_H_

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

//#include <gsl/gsl_rng.h>
//#include <gsl/gsl_randist.h>

#ifndef _OPENMP
#include "omp.h"
#endif

#include "anyoption.h"
#include "Bus.h"
#include "defs.h"
#include "Generator.h"
#include "Line.h"
#include "MTRand.h"
#include "Primes.h"
#include "RandomNumbers.h"

namespace String {
	extern std::string vectorToString(std::vector<double> v);
	extern std::string vectorToString(std::vector<int> v);
	extern std::string arrayToString(int* v, int size);
	extern void tokenizeString(std::string str, std::vector<std::string>& tokens, const std::string& delimiter);
};
#endif /*STRING_H_*/