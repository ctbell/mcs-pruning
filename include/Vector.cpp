#include "Utils.h"
#include "Vector.h"
#include <iostream>
#include "Eigen/Dense"

namespace Vector {
	void printVector(std::vector < std::vector < double > >& v, std::string title, int precision) {
		std::cout << title << std::endl;
		for (int i = 0; i<(int)v.size(); i++) {
			for (int j = 0; j<(int)v[i].size(); j++) {
				std::cout << setprecision(precision) << v[i][j] << " ";
			}
			std::cout << std::endl;
		}
	}
	void printVector(std::vector < std::vector < std::vector < double > > >& v, std::string title, int precision) {
		std::cout << title << std::endl;
		for (int i = 0; i<(int)v.size(); i++) {
			for (int j = 0; j<(int)v[i].size(); j++) {
				for (int k = 0; k<(int)v[i][j].size(); k++) {
					std::cout << setprecision(precision) << v[i][j][k] << " ";
				}
			}
			std::cout << std::endl;
		}
	}
}
