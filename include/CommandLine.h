#ifndef CMD_H_
#define CMD_H_

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iterator>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

//#include <gsl/gsl_rng.h>
//#include <gsl/gsl_randist.h>

#ifndef _OPENMP
#include "omp.h"
#endif

#include "anyoption.h"
#include "Bus.h"
#include "defs.h"
#include "Generator.h"
#include "Line.h"
#include "MTRand.h"
#include "Primes.h"
#include "RandomNumbers.h"

namespace CommandLine {
	extern void setUsage(AnyOption* opt);
	extern void setOptions(AnyOption* opt);
};
#endif /*CMD_H_*/